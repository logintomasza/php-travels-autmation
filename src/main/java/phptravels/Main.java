package phptravels;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class Main {


    public static void main(String[] args){
        System.out.print("Hello World");
        dateParser();
    }


    public static LocalDate dateParser() {
        String arrivalInterDateText = "Thursday 21 May 2020 16:30"; //Thursday 21 May 2020 16:30
        DateTimeFormatter searchResultsFormatter = DateTimeFormatter.ofPattern("EEEE d MMMM yyyy HH:mm", Locale.ENGLISH);
        LocalDate parsedDate = LocalDate.parse(arrivalInterDateText, searchResultsFormatter);
        return parsedDate;
    }

}
