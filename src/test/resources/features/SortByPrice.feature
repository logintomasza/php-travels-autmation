Feature: Booking

Background: Prepare WebSite
  And user is on Home Page
  #And user is logged in with credentials: "user@phptravels.com", "demouser"

@sort-price
  Scenario Outline: Sort By price
  And user selects Flights section
  And user flies from "<From>"
  And user flies to "<To>"
  And user selects date to <DateFromToday> date
  And user selects <Adults> Adults
  And user selects <Children> children
  And user clicks Search button
  Then displayed results are sorted by price
  Then start point is always "<From>"
  Then end point is always "<To>"

  Examples:
  | From                    | To            | Adults | Children | DateFromToday     |  Login                    | Password |
  | New York                | Munich        | 2      | 2        | 14                |    user@phptravels.com    | demouser |


@booking-information
  Scenario Outline: Verify booking information
    And user selects Flights section
    And user flies from "<From>"
    And user flies to "<To>"
    And user selects date to <DateFromToday> date
    And user selects <Adults> Adults
    And user selects <Children> children
    And user clicks Search button
    And user selects first flight on the list
    And user goes to Checkout page
    Then details "DepartureDate", "ArrivalDate" nad "Price" are correctly displayed
    Examples:
      | From                    | To            | Adults | Children | DateFromToday     |  Login                    | Password |
      | New York                | Munich        | 2      | 2        | 14                |    user@phptravels.com    | demouser |

