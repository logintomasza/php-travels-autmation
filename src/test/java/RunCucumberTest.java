import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome=true,
                features = "src\\test\\resources\\features\\SortByPrice.feature",
                glue = {"Tests"},
                tags= {""})
public class RunCucumberTest {
}
