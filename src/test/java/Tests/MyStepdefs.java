package Tests;

import Tests.PageObjects.CheckoutPage;
import Tests.PageObjects.FlightsSearchResults;
import Tests.PageObjects.Locators;
import Tests.PageObjects.FlightsSearchFrame;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static Tests.BaseTest.HOME_URL;
import static Tests.BaseTest.driver;
import static Tests.PageObjects.FlightsSearchFrame.*;
import static Tests.PageObjects.FlightsSearchResults.*;



public class MyStepdefs{

    String departureDate;

    @And("user is on Home Page")
    public void userIsOnHomePage() {
        driver.get(HOME_URL);
    }

    @And("user selects Flights section")
    public void userSelectsFlightsSection() {
        flightsTab.click();
    }

    @And("user flies from {string}")
    public void userFliesFrom(String arg0) {
        fromInputContainer.click();
        fromInputField.sendKeys(arg0);
        List<WebElement> searchResults = driver.findElements(Locators.searchResultsList);
        searchResults.get(0).click();
    }

    @And("user flies to {string}")
    public void userFliesToTo(String arg0) {
        toInputContainer.click();
        toInputField.sendKeys(arg0);
        List<WebElement> searchResults2 = driver.findElements(Locators.searchResultsList);
        searchResults2.get(0).click();
    }

    @And("user selects date to {int} date")
    public void userSelectsDateToDateFromTodayDate(int arg0) {
        departureDate = selectDate(arg0);
    }

    @And("user selects {int} Adults")
    public void userSelectsAdultsAdults(int arg0) {
        FlightsSearchFrame.addAdults(arg0);
    }

    @And("user selects {int} children")
    public void userSelectsChildrenChildren(int arg0) {
        FlightsSearchFrame.addChildren(arg0);
    }

    @And("user clicks Search button")
    public void userClicksSearchButton() {
        searchButton.click();
    }

    @Then("displayed results are sorted by price")
    public void displayedResultsAreSortedByPrice() {
        ArrayList<Integer> retrievedList = FlightsSearchResults.getPricesList();
        ArrayList<Integer> sortedList = FlightsSearchResults.getSortedList(retrievedList);
        Assert.assertTrue(retrievedList.size() > 0);
        Assert.assertEquals(retrievedList, sortedList);
    }

    @Then("start point is always {string}")
    public void startPointIsAlways(String arg0) {
        verifyPoints(arg0, startPointElements);
    }

    @Then("end point is always {string}")
    public void endPointIsAlways(String arg0) {
        verifyPoints(arg0, endPointElements);
    }


    HashMap<String, LocalDate> searchDetails;
    HashMap<String, LocalDate> bookingDetails;
    @And("user selects first flight on the list")
    public void userSelectsFirstFlightOnTheList() {
        searchDetails = getSearchDetails();
    }

    @And("user goes to Checkout page")
    public void userGoesToCheckoutPage() {
        clickBookNowButton();
    }

    @Then("details {string}, {string} nad {string} are correctly displayed")
    public void detailsNadOnBookingSummaryAreTheSameAsOnResultsPage(String arg0, String arg1, String arg2) {
        bookingDetails = CheckoutPage.getBookingDetails();

        Assert.assertEquals(searchDetails.get(arg0), bookingDetails.get(arg0));
        Assert.assertEquals(searchDetails.get(arg1), bookingDetails.get(arg1));
        //Assert.assertEquals(searchDetails.get(arg2), bookingDetails.get(arg2));

        Assert.assertEquals(Integer.parseInt(CheckoutPage.baseRate.getText().replaceAll("[^\\d.]", ""))
                + Integer.parseInt(CheckoutPage.surcharges.getText().replaceAll("[^\\d.]", "")),
                Integer.parseInt((CheckoutPage.totalAmount.getText().replaceAll("[^\\d.]", ""))));

    }

}
