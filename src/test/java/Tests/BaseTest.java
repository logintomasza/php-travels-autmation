package Tests;

import io.cucumber.java.*;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;


public class BaseTest {

    public static WebDriver driver;
    public final static String HOME_URL = "https://www.phptravels.net/home";

    @Before
    public void prepare() {
        //setup chromedriver
//        System.setProperty(
//                "webdriver.chrome.driver",
//                "webdriver/chromedriver");

        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(HOME_URL);
        driver.manage().window().maximize();
    }


    @After
    public void teardown() throws IOException {
        driver.close();
    }
}
