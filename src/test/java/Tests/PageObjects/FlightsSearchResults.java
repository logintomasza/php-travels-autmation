package Tests.PageObjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static Tests.BaseTest.driver;

public class FlightsSearchResults {
    public static List<WebElement> priceListElements = driver.findElements(Locators.priceTagClass);
    public static List<WebElement> startPointElements = driver.findElements(Locators.startPointdetailName);
    public static List<WebElement> endPointElements = driver.findElements(Locators.endPointdetailName);
    public static List<WebElement> flightTimes = driver.findElements(By.className("theme-search-results-item-flight-section-meta-time"));
    public static List<WebElement> flightDates = driver.findElements(By.className("theme-search-results-item-flight-section-meta-date"));
    public static List<WebElement> bookNowButtons = driver.findElements(By.xpath("//button[contains(text(),\"Book Now\")]"));



    public static ArrayList<Integer> getPricesList() {
        ArrayList<Integer> priceList = new ArrayList<>();
        for (WebElement priceTag: priceListElements) {
            priceList.add(Integer.parseInt(priceTag.getText().replaceAll("[^\\d.]", "")));
        }
        return priceList;
    }

    public static void verifyStartPoints(String textToVerify) {
        for(WebElement startPoint : startPointElements) {
            Assert.assertEquals(startPoint.getText(), textToVerify);
        }
    }

    public static void verifyEndPoints(String textToVerify) {
        for(WebElement startPoint : endPointElements) {
            Assert.assertEquals(startPoint.getText(), textToVerify);
        }
    }
    public static void verifyPoints(String textToVerify, List<WebElement> elements) {
        for(WebElement startPoint : elements) {
            Assert.assertEquals(startPoint.getText(), textToVerify);
        }
    }

    public static ArrayList<Integer> getSortedList(ArrayList<Integer> list) {
        Collections.sort(list);
        return list;
    }

    public static HashMap<String, LocalDate> getSearchDetails() {
        String departureHour = flightTimes.get(0).getText().replace(" ", "");
        String arrivalHour = flightTimes.get(1).getText().replace(" ", "");
        String departureInterDateText = flightDates.get(0).getText() + " " + departureHour;
        String arrivalInterDateText = flightDates.get(1).getText() + " " + arrivalHour; //Thursday 21 May 2020 16:30

        DateTimeFormatter searchResultsFormatter = DateTimeFormatter.ofPattern("EEEE d MMMM yyyy HH:mm", Locale.ENGLISH);

        LocalDate departureInterDate = LocalDate.parse(departureInterDateText, searchResultsFormatter);
        LocalDate arrivalInterDate = LocalDate.parse(arrivalInterDateText, searchResultsFormatter);

        HashMap<String, LocalDate> datesMap = new HashMap<>();
        datesMap.put("DepartureDate", departureInterDate);
        datesMap.put("ArrivalDate", arrivalInterDate);
        return datesMap;
    }

    public static void clickBookNowButton() {
        bookNowButtons.get(0).click();
    }




}
