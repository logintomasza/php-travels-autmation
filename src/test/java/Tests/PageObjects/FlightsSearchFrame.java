package Tests.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.text.SimpleDateFormat;
import java.util.*;

import static Tests.BaseTest.driver;
import static Tests.PageObjects.Locators.dayBaseXpath;
import static Tests.PageObjects.Locators.monthBaseXpath;

public class FlightsSearchFrame {

    public static WebElement searchButton = driver.findElement(Locators.searchButton);
    public static WebElement fromInputField = driver.findElement(Locators.locationFromInput);
    public static WebElement toInputField = driver.findElement(Locators.locationToInput);
    public static WebElement fromInputContainer = driver.findElement(Locators.locationFromContainer);
    public static WebElement toInputContainer = driver.findElement(Locators.locationToContainer);
    public static WebElement flightsTab = driver.findElement(Locators.flightsTabButton);

    public static WebElement adultPlusButton = driver.findElement(Locators.adultsPlusSign);
    public static WebElement adultMinusButton = driver.findElement(Locators.adultsMinusSign);
    public static WebElement childPlusButton = driver.findElement(Locators.childPlusSign);
    public static WebElement childMinusButton = driver.findElement(Locators.childMinusSign);

    public static WebElement numOfAdultsValueField = driver.findElement(Locators.numOfAdultsValue);

    public static WebElement flightDepartDateInput = driver.findElement(Locators.flightDepartDateInput);
    public static WebElement calendarTitle = driver.findElement(Locators.datePickerTitle);



    public static void addAdults(int numOfAdults) {
        int adultsAlreadyAdded = Integer.parseInt(numOfAdultsValueField.getAttribute("value"));
        for (int i = 0; i < numOfAdults-adultsAlreadyAdded; i++) {
            adultPlusButton.click();
        }
    }

    public static void addChildren(int numOfChildren) {
        for (int i = 0; i < numOfChildren; i++) {
            childPlusButton.click();
        }
    }

    public static String selectDate(int daysInFuture) {
        flightDepartDateInput.click();
        calendarTitle.click();
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, daysInFuture);
        int departMonth = c.get( c.MONTH);
        int departDay = c.get(c.DAY_OF_MONTH);
        String newMonthXpath = monthBaseXpath.replace("<int_placeholder>", String.valueOf(departMonth));
        String newDayXpath = dayBaseXpath.replace("<int_placeholder>", String.valueOf(departDay));
        driver.findElement(By.xpath(newMonthXpath)).click();
        driver.findElement(By.xpath(newDayXpath)).click();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy\\MM\\dd");
        Date departureDate = new Calendar.Builder().setDate(c.get(c.YEAR), departMonth, departDay).build().getTime();
        String departureDateString = simpleDateFormat.format(departureDate);
        return departureDateString;
    }

}
