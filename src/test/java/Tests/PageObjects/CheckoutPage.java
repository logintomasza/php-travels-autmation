package Tests.PageObjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static Tests.BaseTest.driver;

public class CheckoutPage {
    public static List<WebElement> priceListElements = driver.findElements(Locators.priceTagClass);
    public static WebElement departureInterDateText = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[1]/div/div/div[1]/aside/div[2]/div/ul[1]/li[1]/span"));
    public static WebElement arrivalInterDateText = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[1]/div/div/div[1]/aside/div[2]/div/ul[1]/li[2]/span"));
    public static WebElement baseRate = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[1]/div/div/div[1]/aside/div[2]/div/ul[2]/li[1]/span"));
    public static WebElement surcharges = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[1]/div/div/div[1]/aside/div[2]/div/ul[2]/li[2]/span"));
    public static WebElement totalAmount = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[1]/div/div/div[1]/aside/div[2]/div/ul[2]/li[3]/span"));



    public static HashMap<String, LocalDate> getBookingDetails() {
        DateTimeFormatter bookingSummaryFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        LocalDate departureInterDate2 = LocalDate.parse(departureInterDateText.getText(), bookingSummaryFormatter);
        LocalDate arrivalInterDate2 = LocalDate.parse(arrivalInterDateText.getText(), bookingSummaryFormatter);

        HashMap<String, LocalDate> datesMap = new HashMap<>();
        datesMap.put("DepartureDate", departureInterDate2);
        datesMap.put("ArrivalDate", arrivalInterDate2);
        return datesMap;
    }







}
