package Tests.PageObjects;

import org.openqa.selenium.By;

public class Locators {

    public static By searchButton = By.cssSelector("#flights > div > div > form > div > div.row.no-gutters.mb-15.row-reverse > div.col-xs-12.col-md-12 > button");
    public static By searchResultsList = By.className("select2-result-label");
    public static By flightsTabButton = By.cssSelector("a.text-center.flights");
    public static By locationFromContainer = By.id("s2id_location_from");
    public static By locationToContainer = By.id("s2id_location_to");
    public static By locationFromInput = By.id("location_from");
    public static By locationToInput = By.id("location_to");

    public static By adultsPlusSign = By.cssSelector("#flights > div > div > form > div > div.row.no-gutters.mb-15.row-reverse > div.col-md-4.col-xs-12 > div > div > div:nth-child(1) > div > div.form-icon-left > div > span > button.btn.btn-white.bootstrap-touchspin-up");
    public static By adultsMinusSign = By.cssSelector("#flights > div > div > form > div > div.row.no-gutters.mb-15.row-reverse > div.col-md-4.col-xs-12 > div > div > div:nth-child(1) > div > div.form-icon-left > div > span > button.btn.btn-white.bootstrap-touchspin-down");

    public static By childPlusSign = By.cssSelector("#flights > div > div > form > div > div.row.no-gutters.mb-15.row-reverse > div.col-md-4.col-xs-12 > div > div > div:nth-child(2) > div > div.form-icon-left > div > span > button.btn.btn-white.bootstrap-touchspin-up");
    public static By childMinusSign = By.cssSelector("#flights > div > div > form > div > div.row.no-gutters.mb-15.row-reverse > div.col-md-4.col-xs-12 > div > div > div:nth-child(2) > div > div.form-icon-left > div > span > button.btn.btn-white.bootstrap-touchspin-down");

    public static By numOfAdultsValue = By.cssSelector("#flights > div > div > form > div > div.row.no-gutters.mb-15.row-reverse > div.col-md-4.col-xs-12 > div > div > div:nth-child(1) > div > div.form-icon-left > div > input");

    public static By flightDepartDateInput = By.id("FlightsDateStart");
    public static By datePickerNextArrow = By.cssSelector("#datepickers-container > div:nth-child(8) > nav > div:nth-child(3)");
    public static By datePickerTitle = By.cssSelector("#datepickers-container > div:nth-child(8) > nav > div.datepicker--nav-title");

    public static String monthBaseXpath = "//*[@id=\"datepickers-container\"]/div[8]/div/div[2]/div/div[@data-month=\"<int_placeholder>\"]";
    public static String dayBaseXpath = "//*[@id=\"datepickers-container\"]/div[8]/div/div[1]/div[2]/div[@data-date=\"<int_placeholder>\"]";

    public static By priceTagClass = By.className("theme-search-results-item-price-tag");

    public static By takeoffTagClass = By.className("theme-search-results-item-flight-section-path-line-title");

    public static By startPointdetailName = By.xpath("//*[@id=\"LIST\"]/li[1]/div/div[1]/div[2]/form/div[1]/div/div/div/div[2]/div/div/div[2]/div/div[@class=\"theme-search-results-item-flight-section-path-line-start\"]/div[2]");
    public static By endPointdetailName = By.xpath("//*[@id=\"LIST\"]/li[1]/div/div[1]/div[2]/form/div[1]/div/div/div/div[2]/div/div/div[2]/div/div[@class=\"theme-search-results-item-flight-section-path-line-end\"]/div[2]");


}
